namespace SecureWebApi
{
    public class IdentityServerSettings
    {
        public static string Section = "Auth";
        public string Server {get;set;}
        public string Secret {get;set;}
    }
}