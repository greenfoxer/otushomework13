using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;

namespace ConsoleClient
{
    class Worker
    {
        WorkerSettings _workerConfig;
        public Worker(WorkerSettings workerConfig)
        {
            _workerConfig = workerConfig;
        }
        private string AccessToken;

        public string GetToken => AccessToken;

        public bool IsTokenObtained => !string.IsNullOrEmpty(AccessToken);
        public async Task<bool> ObtainAccessToken()
        {
            AccessToken = null;

            var client = new HttpClient();
            var disco = await client.GetDiscoveryDocumentAsync(_workerConfig.AuthServer);
            if (disco.IsError)
            {
                Console.WriteLine(disco.Error);
                return false;
            }

            var tokenResponse = await client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = disco.TokenEndpoint,

                ClientId = _workerConfig.ClientId,
                ClientSecret = _workerConfig.Secret,
                Scope = _workerConfig.Scope
            });

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return false;
            }

            AccessToken = tokenResponse.AccessToken;
            Console.WriteLine($"Token obtained: {AccessToken}");

            return true;
        }
        public async Task<bool> ObtainProtetcedData()
        {
            if(IsTokenObtained)
            {
                var apiClient = new HttpClient();
                apiClient.SetBearerToken(AccessToken);

                var response = await apiClient.GetAsync(_workerConfig.ProtectedResource);
                if (!response.IsSuccessStatusCode)
                {
                    Console.WriteLine(response.StatusCode);
                }
                else
                {
                    var content = await response.Content.ReadAsStringAsync();
                    Console.WriteLine(JArray.Parse(content));
                }

                return true;
            }
            else
                return false;
        }
    }
}
