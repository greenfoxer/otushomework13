namespace ConsoleClient
{
    public class WorkerSettings
    {
        public static string Section = "AuthSettings";
        public string AuthServer {get;set;}
        public string ProtectedResource {get;set;}
        public string ClientId {get;set;}
        public string Secret {get;set;}
        public string Scope {get;set;}
    }
}
