﻿using System.Threading;
using System.IO;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            IConfiguration config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .Build();
            
            var workerConf = new WorkerSettings();
            config.Bind(WorkerSettings.Section, workerConf);

            var worker = new Worker(workerConf);

            Execute(worker).Wait();

        }
        static async Task Execute(Worker worker)
        {
            while(true)
            {
                if(await worker.ObtainAccessToken())
                    await worker.ObtainProtetcedData();
                
                Thread.Sleep(5000);
            }
        }
    }
}
