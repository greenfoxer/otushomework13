﻿// Copyright (c) Brock Allen & Dominick Baier. All rights reserved.
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.


using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System.Collections.Generic;
using System.Security.Claims;

namespace IdentityServer
{
    public static class Config
    {
        public static IEnumerable<IdentityResource> IdentityResources =>
            new IdentityResource[]
            { 
                new IdentityResources.OpenId()
            };

        public static IEnumerable<ApiScope> ApiScopes =>
            new ApiScope[]
            { 
                new ApiScope("otusHomeWorkScope")
            };

        public static IEnumerable<Client> Clients =>
            new Client[] 
            { 
                new Client
                {
                    ClientId = "otusClient",

                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    ClientSecrets =
                    {
                        new Secret("otusClientStrongSecret".Sha256())
                    },

                    AllowedScopes = { "otusHomeWorkScope" }
                }
            };
        public static List<TestUser> TestUsers =>
        new List<TestUser>
        {
            new TestUser
                {
                    SubjectId = "5BE86359-073C-434B-AD2D-A3932222DABE",
                    Username = "user",
                    Password = "password",
                    Claims = new List<Claim>
                    {
                        new Claim("given_name", "User"),
                        new Claim("family_name", "Testing"),
                        new Claim(JwtClaimTypes.Email, "user@mail.com"),
                        new Claim("role", "User")
                    }
                }
        };
    }
}