namespace IdentityServer.Settings
{
    public class FacebookSettings
    {
        public string AppId {get;set;}
        public string AppSecret {get;set;}
    }
}