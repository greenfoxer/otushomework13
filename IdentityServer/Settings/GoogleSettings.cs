namespace IdentityServer.Settings
{
    public class GoogleSettings
    {
        public string ClientId {get;set;}
        public string ClientSecret {get;set;}
    }
}